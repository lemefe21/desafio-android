package com.desafio.leme.desafioandroid;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.desafio.leme.desafioandroid.task.CarregaShotsTask;

public class PrincipalActivity extends ActionBarActivity {

    private int paginacao = 1;
    private ListView listaShots;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        listaShots = (ListView) findViewById(R.id.lista_shots);

        Log.i("LOG_DESAFIO", "onCreate");

        new CarregaShotsTask(this, paginacao, listaShots).execute();

        listaShots.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(PrincipalActivity.this, getString(R.string.posicao) + position, Toast.LENGTH_LONG).show();
            }
        });

        listaShots.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                Log.i("LOG_DESAFIO", "onScrollStateChanged...");
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                Log.i("LOG_DESAFIO", "onScroll...");
            }
        });
    }
}
