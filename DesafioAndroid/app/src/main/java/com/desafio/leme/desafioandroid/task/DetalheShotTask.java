package com.desafio.leme.desafioandroid.task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.desafio.leme.desafioandroid.R;
import com.desafio.leme.desafioandroid.adapter.ListaShotsAdapter;
import com.desafio.leme.desafioandroid.entidade.Pagina;
import com.desafio.leme.desafioandroid.entidade.Shot;
import com.desafio.leme.desafioandroid.support.WebClient;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class DetalheShotTask extends AsyncTask<String, Void, String>{

    private Activity activity;
    private ImageView detalheShotImage;
    private TextView idShotDetalhe;
    private TextView detalheShotTitle;
    private TextView detalheShotViews;
    private Shot shot;

    public DetalheShotTask(Activity activity, Shot shot){
        this.activity = activity;
        this.shot = shot;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        this.detalheShotImage = (ImageView) activity.findViewById(R.id.image_shot_detalhe);
        this.detalheShotTitle = (TextView) activity.findViewById(R.id.shot_title_detalhe);
        this.detalheShotViews = (TextView) activity.findViewById(R.id.shot_views_count_detalhe);

    }

    @Override
    protected String doInBackground(String... params) {

        Log.i("LOG_DESAFIO", "doInBackground - DetalheShotTask");
        return "detalhe_carregado";

    }

    @Override
    protected void onPostExecute(String retornoGet) {
        super.onPostExecute(retornoGet);
        Log.i("LOG_DESAFIO", "DetalheShotTask - onCreateView - " + retornoGet);

    }
}
