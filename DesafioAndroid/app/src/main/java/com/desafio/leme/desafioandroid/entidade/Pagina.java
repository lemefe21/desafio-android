package com.desafio.leme.desafioandroid.entidade;

public class Pagina {

    private String page;
    private int per_page;
    private int pages;
    private int total;
    private Shot[]shots;

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public int getPer_page() {
        return per_page;
    }

    public void setPer_page(int per_page) {
        this.per_page = per_page;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public Shot[] getShots() {
        return shots;
    }

    public void setShots(Shot[] shots) {
        this.shots = shots;
    }

}
