package com.desafio.leme.desafioandroid.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.desafio.leme.desafioandroid.R;
import com.desafio.leme.desafioandroid.ShotsActivity;
import com.desafio.leme.desafioandroid.entidade.Shot;
import com.desafio.leme.desafioandroid.task.DetalheShotTask;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class DetalheShotFragment extends Fragment{

    private Shot shot;
    private ProgressBar progressBar;
    private ImageView detalheShotImage;
    private TextView detalheShotTitle;
    private TextView detalheShotViews;
    private ImageView avatarShotImage;
    private TextView nomeUsuarioShotDetalhe;
    private TextView descricaoShotDetalhe;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View layoutDetalheShot = inflater.inflate(R.layout.fragment_detalhe_shot, container, false);

        if(getArguments() != null){

            this.shot = (Shot) getArguments().getSerializable(ShotsActivity.SHOT);

            buscaComponentesView(layoutDetalheShot);

            populaCamporDetalhes(this.shot);

        }

        return layoutDetalheShot;
    }

    private void buscaComponentesView(View layout){

        this.progressBar = (ProgressBar) layout.findViewById(R.id.progress_shot_image_detalhe);
        this.detalheShotImage = (ImageView) layout.findViewById(R.id.image_shot_detalhe);
        this.detalheShotTitle = (TextView) layout.findViewById(R.id.shot_title_detalhe);
        this.detalheShotViews = (TextView) layout.findViewById(R.id.shot_views_count_detalhe);
        this.avatarShotImage = (ImageView) layout.findViewById(R.id.avater_foto_shot_detalhe);
        this.nomeUsuarioShotDetalhe = (TextView) layout.findViewById(R.id.nome_usuario_shot_detalhe);
        this.descricaoShotDetalhe = (TextView) layout.findViewById(R.id.descricao_shot_detalhe);

        this.progressBar.setVisibility(View.VISIBLE);

    }

    private void populaCamporDetalhes(Shot shot){

        if(shot != null){

            this.detalheShotTitle.setText(shot.getTitle());
            this.detalheShotViews.setText(String.valueOf(shot.getViews_count()));
            this.nomeUsuarioShotDetalhe.setText(shot.getPlayer().getName());

            try {
                this.descricaoShotDetalhe.setText(Html.fromHtml(shot.getDescription()));
            }catch (Exception e){
                this.descricaoShotDetalhe.setText(shot.getDescription());
            }

            if(shot.getImage_400_url() != null){

                Picasso.with(getActivity()).load(shot.getImage_400_url()).into(this.detalheShotImage, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {

                    }
                });
            }else{
                this.detalheShotImage.setImageResource(R.drawable.imagem_shot_default);
            }

            if(shot.getPlayer().getAvatar_url() != null){

                Picasso.with(getActivity()).load(shot.getPlayer().getAvatar_url()).into(this.avatarShotImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        progressBar.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onError() {
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                });
            }else{
                this.avatarShotImage.setImageResource(R.drawable.foto_perfil_default);
            }
        }

    }

}
