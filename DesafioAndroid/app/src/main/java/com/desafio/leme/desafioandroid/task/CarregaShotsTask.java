package com.desafio.leme.desafioandroid.task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.desafio.leme.desafioandroid.adapter.ListaShotsAdapter;
import com.desafio.leme.desafioandroid.entidade.Pagina;
import com.desafio.leme.desafioandroid.entidade.Shot;
import com.desafio.leme.desafioandroid.support.WebClient;
import com.google.gson.Gson;

public class CarregaShotsTask extends AsyncTask<String, Void, String>{

    private Activity activity;
    private String url = "http://api.dribbble.com/shots/popular?page=";
    private int paginacao;
    private ProgressDialog progress;
    private ListView listaShots;

    public CarregaShotsTask(Activity activity, int paginacao, ListView listaShots){
        this.activity = activity;
        this.paginacao = paginacao;
        this.listaShots = listaShots;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        Log.i("LOG_DESAFIO", "onPreExecute");

        progress = ProgressDialog.show(activity, "Dribbble", "Atualizando shots...");

    }

    @Override
    protected String doInBackground(String... params) {

        Log.i("LOG_DESAFIO", "doInBackground");

        String retornoGet = new WebClient(url.concat(String.valueOf(paginacao))).get();

        return retornoGet;
    }

    @Override
    protected void onPostExecute(String retornoGet) {
        super.onPostExecute(retornoGet);

        progress.dismiss();

        if(!retornoGet.equals("ERRO")){

            Gson gson = new Gson();
            Pagina pagina = gson.fromJson(retornoGet, Pagina.class);
            Shot[] shots = pagina.getShots();

            Log.i("LOG_DESAFIO", "onPostExecute - gson.fromJson: " + pagina.getTotal());
            Log.i("LOG_DESAFIO", "onPostExecute - listaShots: " + shots.length);

            listaShots.setAdapter(new ListaShotsAdapter(shots, activity));

        }

    }

}
