package com.desafio.leme.desafioandroid.support;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class WebClient {

    private final String url;

    public WebClient(String url) {
        this.url = url;
    }

    public String get(){

        DefaultHttpClient client = new DefaultHttpClient();
        HttpGet get = new HttpGet(url);
        get.setHeader("Content-type", "application/json");
        get.setHeader("Accept", "application/json");

        try {

            HttpResponse response = client.execute(get);
            StatusLine statusLine = response.getStatusLine();

            Log.i("LOG_DESAFIO", "statusLine.getStatusCode() == " + statusLine.getStatusCode());

            if(statusLine.getStatusCode() == 200){

                HttpEntity entity = response.getEntity();
                String respostaJSON = EntityUtils.toString(entity);
                return respostaJSON;

            }

        }catch (Exception e){
            e.printStackTrace();
            Log.i("LOG_DESAFIO", "Exception: " + e.getMessage());
        }

        return "ERRO";
    }
}
