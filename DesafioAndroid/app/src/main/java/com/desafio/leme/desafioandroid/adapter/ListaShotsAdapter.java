package com.desafio.leme.desafioandroid.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.desafio.leme.desafioandroid.R;
import com.desafio.leme.desafioandroid.entidade.Shot;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class ListaShotsAdapter extends BaseAdapter {

    private Activity activity;
    private Shot[]shots;

    public ListaShotsAdapter(Shot[]shots, Activity activity){
        this.activity = activity;
        this.shots = shots;
    }

    @Override
    public int getCount() {
        return shots.length;
    }

    @Override
    public Object getItem(int position) {
        return shots[position];
    }

    @Override
    public long getItemId(int position) {
        return shots[position].getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        if(convertView == null){

            convertView = activity.getLayoutInflater().inflate(R.layout.shot_layout, null, false);
            holder = new ViewHolder();

            holder.image_400_url = (ImageView) convertView.findViewById(R.id.image_shot);
            holder.title = (TextView) convertView.findViewById(R.id.shot_title);
            holder.views_count = (TextView) convertView.findViewById(R.id.shot_views_count);
            holder.progress = (ProgressBar) convertView.findViewById(R.id.progress_shot_image);

            convertView.setTag(holder);

        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        Shot shot = shots[position];

        if(shot != null){

            String cutTitle = shot.getTitle();
            if(cutTitle.length() > 20){
                cutTitle = cutTitle.substring(0,19).concat("...");
            }

            holder.title.setText(cutTitle);
            holder.views_count.setText(String.valueOf(shot.getViews_count()));

        }

        if(shot.getImage_400_url() != null){

            holder.progress.setVisibility(View.VISIBLE);

            Picasso.with(activity).load(shot.getImage_400_url()).into(holder.image_400_url, new Callback() {
                @Override
                public void onSuccess() {
                    holder.progress.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onError() {
                    holder.progress.setVisibility(View.INVISIBLE);
                }

            });

        }else{
            holder.image_400_url.setImageResource(R.drawable.imagem_shot_default);
        }

        return convertView;
    }

    public class ViewHolder{

        ImageView image_400_url;
        TextView title;
        TextView views_count;
        ProgressBar progress;

    }
}
