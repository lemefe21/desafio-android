package com.desafio.leme.desafioandroid;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;

import com.desafio.leme.desafioandroid.entidade.Shot;
import com.desafio.leme.desafioandroid.fragment.DetalheShotFragment;
import com.desafio.leme.desafioandroid.fragment.ListaShotsFragment;


public class ShotsActivity extends FragmentActivity {

    public static final String SHOT = "shot";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shots);

        Log.i("LOG_DESAFIO", "ShotsActivity - onCreate");

        FragmentTransaction ftx = getSupportFragmentManager().beginTransaction();
        ftx.replace(R.id.shots_view_fragment, new ListaShotsFragment());
        ftx.commit();

    }

    public void selecionaShot(Shot shotSelecionado) {

        Bundle bundleShot = new Bundle();
        bundleShot.putSerializable(SHOT, shotSelecionado);

        DetalheShotFragment detalheFragment = new DetalheShotFragment();
        detalheFragment.setArguments(bundleShot);

        FragmentTransaction ftx = getSupportFragmentManager().beginTransaction();
        ftx.replace(R.id.shots_view_fragment, detalheFragment);
        ftx.addToBackStack(null);
        ftx.commit();

    }
}
