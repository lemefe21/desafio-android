package com.desafio.leme.desafioandroid.fragment;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.desafio.leme.desafioandroid.R;
import com.desafio.leme.desafioandroid.ShotsActivity;
import com.desafio.leme.desafioandroid.entidade.Shot;
import com.desafio.leme.desafioandroid.task.CarregaShotsTask;
import com.desafio.leme.desafioandroid.task.LoadMoreShotsTask;


public class ListaShotsFragment extends Fragment {

    private int paginacao = 1;
    private ListView listaShots;
    private int pageCount = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View layoutListaShots = inflater.inflate(R.layout.fragment_lista_shots, container, false);

        listaShots = (ListView) layoutListaShots.findViewById(R.id.lista_shots_fragment);

        new CarregaShotsTask(getActivity(), paginacao, listaShots).execute();

        listaShots.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {

                Shot shotSelecionado = (Shot) adapter.getItemAtPosition(position);

                ShotsActivity shotActivity = (ShotsActivity) getActivity();
                shotActivity.selecionaShot(shotSelecionado);

            }
        });

        listaShots.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

                int threshold = 1;
                int count = listaShots.getCount();

                if (scrollState == SCROLL_STATE_IDLE) {

                    Log.i("LOG_DESAFIO_LOAD", "listaShots.getLastVisiblePosition() - " + listaShots.getLastVisiblePosition());

                    if (listaShots.getLastVisiblePosition() >= count - threshold && pageCount < 2) {
                        new LoadMoreShotsTask(getActivity(), ++paginacao, listaShots).execute();
                    }
                }


            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }

        });

        return layoutListaShots;
    }

    @Override
    public void onPause() {

        Log.i("LOG_DESAFIO", "ListaShotsFragment - onPause");

        super.onPause();
    }

    @Override
    public void onStart() {

        Log.i("LOG_DESAFIO", "ListaShotsFragment - onStart");

        super.onStart();
    }

    @Override
    public void onStop() {

        Log.i("LOG_DESAFIO", "ListaShotsFragment - onStop");

        super.onStop();
    }
}
